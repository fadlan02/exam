package com.nexsoft;

import java.util.Calendar;
import java.util.Scanner;

public class Kalender {
    int Y;
    int startDayOfMonth = 3;

    int spaces = startDayOfMonth;

    public void display() {

        String[] months = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September",
                "October", "November", "December" };

        // days in number

        int[] days = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        for (int M = 1; M <= 12; M++) {

            // check for leap year
            if ((((Y % 4 == 0) && (Y % 100 != 0)) || (Y % 400 == 0)) && M == 2)
                days[M] = 29;

            // display calendar header
            //// display bulan dan tahun
            System.out.println("======================================");
            System.out.println("\t==== Bulan " + months[M] + " ====");
            System.out.println("");
            System.out.println("   S   M    T    W    T     F    S");

            // spaces required
            spaces = (days[M - 1] + spaces) % 7;

            // print the calendar
            for (int i = 0; i < spaces; i++)
                System.out.print("     ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M]))
                    System.out.println();
            }
        }

        this.Y = Y;
    }

    public void setStartDayOfMonth(int startDayOfMonth) {
        this.startDayOfMonth = startDayOfMonth;
    }

}